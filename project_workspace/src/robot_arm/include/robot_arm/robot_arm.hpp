#ifndef __ROBOT_ARM__
#define __ROBOT_ARM__

#include "ros/ros.h"
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/WrenchStamped.h>
#include <kdl_wrapper/kdl_wrapper.h>
// #include <pr2_controllers_msgs/Pr2GripperCommandAction.h>
// #include <actionlib/client/simple_action_client.h>

// Gripper code from: http://wiki.ros.org/pr2_controllers/Tutorials/Moving%20the%20gripper
// Our Action interface type, provided as a typedef for convenience
// typedef actionlib::SimpleActionClient<pr2_controllers_msgs::Pr2GripperCommandAction> gripperClient;

class gripper{
protected:
  // gripperClient* gripperClient_;
  float maxEffort;
  std::string topic;
public:
  //Action client initialization
  gripper(std::string gripperTopic, float maxEffort);

  ~gripper();

  //Open the gripper
  virtual void open() = 0;

  //Close the gripper
  virtual void close() = 0;
};


class robot_arm
{
  public:
    robot_arm(ros::NodeHandle n, std::string givenName, std::string baseLinkName, std::string linkFrameName,
      std::string tipLinkName, std::string velocityControllerTopic, std::string ftTopic);

    std::string getName();
    std::string getEndEffectorLinkFrame();
    std::string getBaseLinkName();
    std::string getToolFrame();
    std::string getControllerTopic();
    KDL::JntArray jointValues();
    KDL::JntArrayVel jointVelocity();
    int nrJoints();
    KDLWrapper * kdl_wrapper;
    geometry_msgs::WrenchStamped getWrench();
    KDL::Frame getEndEffectorPosition();
    KDL::FrameVel getEndEffectorVelocity();

  protected:
    ros::NodeHandle n;
    std::string name;
    std::string linkFrame, baseLinkName, toolFrame, controllerTopic;
    geometry_msgs::WrenchStamped currWrench;
    KDL::JntArray jntValues;
    KDL::JntArray jntVelocity;
    ros::Subscriber jointStateSub, ftSub;
    std::vector<std::string> jointNamesArray;

    void jointStateCallBack(const sensor_msgs::JointState::ConstPtr &msg);
    void ftCallback(const geometry_msgs::WrenchStamped::ConstPtr &msg);
};

#endif
