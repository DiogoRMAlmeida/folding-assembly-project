#include <robot_arm/robot_arm.hpp>

gripper::gripper(std::string gripperTopic, float maxEffort){ //gripperActionTopic
  this->maxEffort = maxEffort;
  topic = gripperTopic;
  // //Initialize the client for the Action interface to the gripper controller
  // //and tell the action client that we want to spin a thread by default
  // gripperClient_ = new gripperClient(gripperActionTopic, true);
  //
  // //wait for the gripper action server to come up
  // while(!gripperClient_->waitForServer(ros::Duration(5.0))){
  //   ROS_INFO("Waiting for the %s action server to come up", gripperActionTopic.c_str());
  // }
}

gripper::~gripper(){
  // delete gripperClient_;
}

// void gripper::openGripper(){
//   pr2_controllers_msgs::Pr2GripperCommandGoal open;
//   open.command.position = 0.08;
//   open.command.max_effort = maxEffort;
//
//   ROS_INFO("Sending open goal");
//   gripperClient_->sendGoal(open);
//   gripperClient_->waitForResult();
//   if(gripperClient_->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
//     ROS_INFO("The gripper opened!");
//   else
//     ROS_INFO("The gripper failed to open.");
// }

// void gripper::closeGripper(){
//   pr2_controllers_msgs::Pr2GripperCommandGoal squeeze;
//   squeeze.command.position = 0.0;
//   squeeze.command.max_effort = maxEffort;
//
//   ROS_INFO("Sending squeeze goal");
//   gripperClient_->sendGoal(squeeze);
//   gripperClient_->waitForResult();
//   if(gripperClient_->getState() == actionlib::SimpleClientGoalState::SUCCEEDED)
//     ROS_INFO("The gripper closed!");
//   else
//     ROS_INFO("The gripper failed to close.");
// }

robot_arm::robot_arm(ros::NodeHandle n, std::string givenName, std::string baseLinkName, std::string linkFrameName,
  std::string tipLinkName, std::string velocityControllerTopic, std::string ftTopic)
{
  this->n = n;
  kdl_wrapper = new KDLWrapper;
  name = givenName;
  this->baseLinkName = baseLinkName;
  toolFrame = tipLinkName;
  linkFrame = linkFrameName;
  controllerTopic = velocityControllerTopic;
  int misses = 0;

  ROS_DEBUG("Successfully created a robot_arm object with name %s!", name.c_str());
  ROS_DEBUG("Base link name: %s\nTip link name: %s", baseLinkName.c_str(), linkFrame.c_str());

  if(!kdl_wrapper->init(baseLinkName, linkFrame))
  {
      ROS_ERROR("Error initializing KDL Wrapper");
  }

  jointStateSub = n.subscribe(std::string("/joint_states"), 1, &robot_arm::jointStateCallBack, this);
  ftSub = n.subscribe(ftTopic, 1, &robot_arm::ftCallback, this);

  KDL::Chain chain = kdl_wrapper->getKDLChain();

  jointNamesArray.resize(chain.getNrOfJoints());
  jntValues.resize(chain.getNrOfJoints());
  jntVelocity.resize(chain.getNrOfJoints());

  for(int i = 0; i < chain.getNrOfJoints(); i++)
  {
    jntValues(i) = 0;
    jntVelocity(i) = 0;
  }

  ROS_DEBUG("Joint arrays have size %d", chain.getNrOfJoints());

  for (int i = 0; i < chain.getNrOfJoints() + misses; i++)
  {
    if(chain.getSegment(i).getJoint().getType() != KDL::Joint::None)
    {
      ROS_DEBUG("Segment joint name: %s", chain.getSegment(i).getJoint().getName().c_str());
      jointNamesArray[i-misses] = chain.getSegment(i).getJoint().getName();
    }
    else
    {
      misses++;
    }
  }
}

int robot_arm::nrJoints()
{
  return kdl_wrapper->getKDLChain().getNrOfJoints();
}

std::string robot_arm::getName()
{
  return name;
}

std::string robot_arm::getEndEffectorLinkFrame()
{
  return linkFrame;
}

std::string robot_arm::getBaseLinkName()
{
    return baseLinkName;
}

std::string robot_arm::getToolFrame()
{
  return toolFrame;
}

std::string robot_arm::getControllerTopic()
{
  return controllerTopic;
}

KDL::JntArray robot_arm::jointValues()
{
  return jntValues;
}

KDL::JntArrayVel robot_arm::jointVelocity()
{
  KDL::JntArrayVel out(jntValues, jntVelocity);

  return out;
}

KDL::Frame robot_arm::getEndEffectorPosition()
{
  KDL::Frame out;
  kdl_wrapper->fk_solver_pos->JntToCart(jntValues, out);

  return out;
}

KDL::FrameVel robot_arm::getEndEffectorVelocity()
{
  KDL::FrameVel out;
  KDL::JntArrayVel jointVel = jointVelocity();

  kdl_wrapper->fk_solver_vel->JntToCart(jointVel, out);

  return out;
}

void robot_arm::jointStateCallBack(const sensor_msgs::JointState::ConstPtr& msg)
{
  ROS_DEBUG_ONCE("Entered the joint states callback!");
  int misses = 0;
  std::vector<std::string>::iterator iter;

  for (int i = 0; i < nrJoints() + misses && i < msg->position.size(); i++)
  {
    iter = std::find(jointNamesArray.begin(), jointNamesArray.end(), msg->name[i]);
    if(iter!=jointNamesArray.end())
    {
      this->jntValues(std::distance(jointNamesArray.begin(), iter)) = msg->position[i];
      this->jntVelocity(std::distance(jointNamesArray.begin(), iter)) = msg->velocity[i];
    }
    else
    {
      misses++;
    }
  }
}

void robot_arm::ftCallback(const geometry_msgs::WrenchStamped::ConstPtr &msg)
{
  currWrench = *msg;
}

geometry_msgs::WrenchStamped robot_arm::getWrench()
{
  return currWrench;
}
