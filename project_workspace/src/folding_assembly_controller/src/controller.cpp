#include <folding_assembly_controller/controller.hpp>

foldingControllerBase::foldingControllerBase(ros::NodeHandle &n, robot_arm *arm_1, robot_arm *arm_2, double desiredNormalForce)
{
  arm1 = arm_1;
  arm2 = arm_2;

  fnRef = desiredNormalForce;

  _n = n;

  monitorPub = n.advertise<folding_assembly_controller::monitorMsg>("foldingController/signals", 1);
}

/*
  Computes the skew-symmetric matrix of the provided vector
*/
Eigen::Matrix3d foldingControllerBase::computeSkewSymmetric(Eigen::Vector3d v)
{
  Eigen::Matrix3d S;

  S << 0,    -v(2),  v(1),
       v(2),  0   , -v(0),
      -v(1),  v(0),  0;

  return S;
}

/*
  Publishes debug information of the folding controller
*/
void foldingControllerBase::publishInfo()
{
  folding_assembly_controller::monitorMsg msg;
  geometry_msgs::WrenchStamped wrench;
  geometry_msgs::Vector3 tempForce;
  KDL::FrameVel fv;

  msg.header.stamp = ros::Time::now();
  tf::vectorEigenToMsg(v1, msg.v1);
  tf::vectorEigenToMsg(vf, msg.vf);
  tf::vectorEigenToMsg(w1, msg.w1);
  tf::vectorEigenToMsg(p1, msg.p1);
  tf::vectorEigenToMsg(p2, msg.p2);
  tf::vectorEigenToMsg(pc, msg.pc);
  tf::vectorEigenToMsg(pd, msg.pd);
  tf::vectorEigenToMsg(surfaceNormal, msg.surfaceNormal);
  tf::vectorEigenToMsg(surfaceTangent, msg.surfaceTangent);
  msg.thetaC = thetaC;
  wrench = arm1->getWrench();
  msg.f1 = wrench.wrench.force;
  msg.torque1 = wrench.wrench.torque;

  // THIS SHOULD BE MADE INTO A FUNCTION
  wrench = arm2->getWrench();
  geometry_msgs::Vector3Stamped forceStamped, torqueStamped;
  forceStamped.header = wrench.header;
  torqueStamped.header = wrench.header;
  forceStamped.vector = wrench.wrench.force;
  torqueStamped.vector = wrench.wrench.torque;
  tfListener.waitForTransform(arm2->getBaseLinkName(), wrench.header.frame_id, ros::Time::now(), ros::Duration(1.0));
  tfListener.transformVector(arm2->getBaseLinkName(), forceStamped, forceStamped);
  tfListener.transformVector(arm2->getBaseLinkName(), torqueStamped, torqueStamped);
  msg.f2 = forceStamped.vector;
  msg.torque2 = torqueStamped.vector;

  fv = arm1->getEndEffectorVelocity();
  tf::vectorKDLToMsg(fv.p.v, msg.vel1);
  tf::vectorKDLToMsg(fv.M.w, msg.omega1);

  monitorPub.publish(msg);
}

/*
  Calls TF to get the contact point ground truth and the grasping point positions in the
  base frame
*/
void foldingControllerBase::getPoints(Eigen::Vector3d &realPc, Eigen::Vector3d &_p1, Eigen::Vector3d &_p2)
{
  tf::StampedTransform transform;

  tfListener.lookupTransform(arm1->getBaseLinkName(), "/rodTip", ros::Time(0), transform);
  realPc(0) = transform.getOrigin().x();
  realPc(1) = transform.getOrigin().y();
  realPc(2) = transform.getOrigin().z();
  // Contact point in the base frame

  tfListener.lookupTransform(arm1->getBaseLinkName(), arm1->getToolFrame(), ros::Time(0), transform);
  _p1(0) = transform.getOrigin().x();
  _p1(1) = transform.getOrigin().y();
  _p1(2) = transform.getOrigin().z();
  // Left gripper position in the base frame

  tfListener.lookupTransform(arm1->getBaseLinkName(), arm2->getToolFrame(), ros::Time(0), transform);
  _p2(0) = transform.getOrigin().x();
  _p2(1) = transform.getOrigin().y();
  _p2(2) = transform.getOrigin().z();
  // Right gripper position in the base frame
}

/*
  Returns the tool frame for the given arm
*/
KDL::Frame foldingControllerBase::toolFrame(robot_arm * arm)
{
  tf::StampedTransform transform;
  KDL::Frame toolFrame;

  tfListener.lookupTransform(arm2->getBaseLinkName(), arm->getToolFrame(), ros::Time(0), transform);
  tf::transformTFToKDL(transform, toolFrame);

  return toolFrame;
}

void foldingControllerBase::setDesiredNormalForce(double newNormalForce)
{
  fnRef = newNormalForce;

  ROS_INFO("Reference force for the folding assembly controller changed to %.3f!", fnRef);
}

void foldingControllerNoEstimate::updateR2()
{
  getPoints(pc, p1, p2);
  r2 = pc - p2;
}

void foldingControllerNoEstimate::updateSurfaceTangent()
{
  KDL::Frame frame = toolFrame(arm2);

  surfaceTangent(0) = frame.M.UnitX().x();
  surfaceTangent(1) = frame.M.UnitX().y();
  surfaceTangent(2) = frame.M.UnitX().z();
}

void foldingControllerNoEstimate::updateSurfaceNormal()
{
  KDL::Frame frame = toolFrame(arm2);

  surfaceNormal(0) = frame.M.UnitZ().x();
  surfaceNormal(1) = frame.M.UnitZ().y();
  surfaceNormal(2) = frame.M.UnitZ().z();
}

void foldingControllerNoEstimate::updateTheta()
{
  getPoints(pc, p1, p2);
  thetaC = atan2(surfaceNormal.dot(r1), r2.dot(r1));
}

void foldingControllerNoEstimate::updateContactPoint()
{
  getPoints(pc, p1, p2);
}

void foldingControllerNoEstimate::updateVelocityTerm()
{
  geometry_msgs::WrenchStamped wrench = arm2->getWrench();
  geometry_msgs::Vector3Stamped forceWorld, forceLocal;
  Eigen::Vector3d force;
  double k_p = 0*0.001, k_i = 0*0.00005;

  forceLocal.vector = wrench.wrench.force;
  forceLocal.header = wrench.header;

  // tfListener.waitForTransform(arm2->getBaseLinkName(), forceLocal.header.frame_id, ros::Time::now(), ros::Duration(1.0));
  tfListener.transformVector(arm1->getToolFrame(), ros::Time(0), forceLocal, forceLocal.header.frame_id, forceWorld);
  tf::vectorMsgToEigen(forceWorld.vector, force);

  vf = force.dot(surfaceNormal)*surfaceNormal + fnRef*surfaceNormal;

  vfAcc += vf*0.01;

  ROS_INFO("Force: %.2f", force.dot(surfaceNormal));
  ROS_INFO("Desired: %.2f", (fnRef*surfaceNormal).dot(surfaceNormal));

  vf = -k_p*vf - k_i*vfAcc;
}

/*
  Performs the folding movement, using the contact point defined in the package
  launch file.
*/
void foldingControllerNoEstimate::control(const double &vd, const double &wd, Eigen::Vector3d &vOut, Eigen::Vector3d &wOut)
{
  Eigen::Matrix3d S;
  Eigen::Vector3d rotationAxis, omegaD, velD;

  updateSurfaceTangent();
  updateSurfaceNormal();

  // rotationAxis = surfaceTangent.cross(surfaceNormal);

  KDL::Frame frame = toolFrame(arm1);

  rotationAxis(0) = frame.M.UnitZ().x();
  rotationAxis(1) = frame.M.UnitZ().y();
  rotationAxis(2) = frame.M.UnitZ().z();

  ROS_INFO("axis ");
  for(int i = 0; i < 3; i++)
    std::cout << rotationAxis(i) << " ";
  std::cout << std::endl;

  omegaD = wd*rotationAxis;
  velD = vd*surfaceTangent;

  getPoints(pc, p1, p2);

  w1 = omegaD;

  r1 = pc - p1;

  // ROS_INFO("p1 ");
  // for(int i = 0; i < 3; i++)
  //   std::cout << p1(i) << " ";
  // std::cout << std::endl;
  //
  // ROS_INFO("pc ");
  // for(int i = 0; i < 3; i++)
  //   std::cout << pc(i) << " ";
  // std::cout << std::endl;

  updateTheta();
  updateVelocityTerm();

  S = computeSkewSymmetric(w1);
  v1 =  - S * r1 + velD + vf;

  if (v1.dot(surfaceTangent) > 0.01)
  {
    v1 = v1/v1.norm() * 0.01;
    ROS_WARN("V1 IS SATURATED");
  }
  if (w1.norm() > 0.1)
  {
    w1 = w1/w1.norm() * 0.1;
    ROS_WARN("OMEGA1 IS SATURATED");
  }

  vOut = v1;
  wOut = w1;

  // output values in the world frame!

  publishInfo();
}

// void foldingControllerEstimate::control(const Eigen::Vector3d &vd, const Eigen::Vector3d &wd, Eigen::Vector3d &vOut, Eigen::Vector3d &wOut)
// {
//   Eigen::Vector3d realR2;
//   Eigen::Vector3d realPc;
//   Eigen::Vector3d realR1;
//   Eigen::Vector3d pe;
//   Eigen::Vector3d axis;
//   double realThetaC;
//   double thetaE;
//   Eigen::Matrix3d S;
//
//   tf::StampedTransform transform;
//
//   getPoints(realPc, p1, p2);
//
//   KDL::Frame frame = arm1->getEndEffectorPosition();
//   axis(0) = frame.M.UnitY().x();
//   axis(1) = frame.M.UnitY().y();
//   axis(2) = frame.M.UnitY().z();
//   // Y axis of the gripper frame in the world frame
//
//   wref = wd; // Omega 1 should be around the left gripper Y axis
//
//   updateR2();
//   pc = r2 + p2;
//   pc(0) = realPc(0);
//   pc(2) = realPc(2);
//   r1 = pc - p1;
//   pe = - pd; // position error IN THE WORLD FRAME
//
//   updateTheta();
//
//   updateVelocityTerm();
//
//   S = computeSkewSymmetric(wref);
//   vref =  - S * (pc - p1) - Kp*pe + vf; // Y in the gripper frame (x in the world frame) times vector along YZ in the world frame will give rise to movement along the normal YZ plane? Minus a term in the world frame Y direction
//
//   if (vref.norm() > 0.1)
//   {
//     vref = vref/vref.norm() * 0.1;
//     ROS_WARN("V1 IS SATURATED");
//   }
//   if (wref.norm() > 0.2)
//   {
//     wref = wref/wref.norm() * 0.2;
//     ROS_WARN("OMEGA1 IS SATURATED");
//   }
//
//   vOut = v1;
//   wOut = w1;
//   publishInfo();
// }
//
//
// void foldingControllerMatrixEstimate::control(const Eigen::Vector3d &vd, const Eigen::Vector3d &wd, Eigen::Vector3d &vOut, Eigen::Vector3d &wOut)
// {
//   Eigen::Vector3d realPc;
//   Eigen::Vector3d pe;
//   Eigen::Vector3d axis;
//   Eigen::Matrix<double, 6, 6> A;
//   Eigen::Matrix<double, 6, 1> out, in;
//   double thetaE;
//   Eigen::Matrix3d S;
//   tf::StampedTransform transform;
//
//   getPoints(realPc, p1, p2);
//
//   KDL::Frame frame = arm1->getEndEffectorPosition();
//   axis(0) = frame.M.UnitY().x();
//   axis(1) = frame.M.UnitY().y();
//   axis(2) = frame.M.UnitY().z();
//   // Y axis of the gripper frame in the world frame
//
//   updateR2();
//   pc = r2 + p2;
//   r1 = pc - p1;
//   pe = - pd; // position error IN THE WORLD FRAME
//
//   updateTheta();
//   updateVelocityTerm();
//
//   S = computeSkewSymmetric(pc - p1);
//   vref = - pe + vf;
//   wref = wd; // Omega 1 should be around the left gripper Y axis
//
//   A.block(0,0,6,6).setIdentity(6,6);
//   A.block(0,3,3,3) = -S;
//
//   in.block(0,0,3,1) = vref;
//   in.block(3,0,3,1) = wref;
//
//   out = A.inverse()*in;
//
//   v1 = out.block(0,0,3,1);
//   w1 = out.block(3,0,3,1);
//
//   if (v1.norm() > 0.1)
//   {
//     v1 = v1/v1.norm() * 0.1;
//     ROS_WARN("V1 IS SATURATED");
//   }
//   if (w1.norm() > 0.2)
//   {
//     w1 = w1/w1.norm() * 0.2;
//     ROS_WARN("OMEGA1 IS SATURATED");
//   }
//
//   vOut = v1;
//   wOut = w1;
//   publishInfo();
// }
