#ifndef __FOLDING_CONTROLLER__
#define __FOLDING_CONTROLLER__
#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>
#include <kdl_conversions/kdl_msg.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/WrenchStamped.h>
#include <folding_assembly_controller/monitorMsg.h>
#include <tf/transform_listener.h>
#include <Eigen/Dense>
#include <math.h>
#include <robot_arm/robot_arm.hpp>
#include <tf_conversions/tf_kdl.h>

/*
Folding Controller Base

Class that defines the base methods and variables to be used in a folding controller implementation.
*/
class foldingControllerBase
{
  public:
    foldingControllerBase(ros::NodeHandle &n, robot_arm *arm_1, robot_arm *arm_2, double desiredNormalForce);
    foldingControllerBase(){}
    virtual void control(const double &vd, const double &wd, Eigen::Vector3d &vOut, Eigen::Vector3d &wOut) = 0;
    void setDesiredNormalForce(double newNormalForce);

  protected:
    Eigen::Vector3d surfaceNormal, surfaceTangent, p1, p2, r1, r2, omega1, pc, pd, thetaD, v1, w1, vref, wref, vf, vfAcc;
    double thetaC, fnRef;
    robot_arm *arm1, *arm2;
    tf::TransformListener tfListener;
    ros::NodeHandle _n;
    ros::Publisher monitorPub;

    Eigen::Matrix3d computeSkewSymmetric(Eigen::Vector3d v);

    virtual void updateR2() = 0;
    virtual void updateSurfaceTangent() = 0;
    virtual void updateSurfaceNormal() = 0;
    virtual void updateTheta() = 0;
    virtual void updateContactPoint() = 0;
    void publishInfo();
    virtual void updateVelocityTerm() = 0;
    void getPoints(Eigen::Vector3d &realPc, Eigen::Vector3d &p1, Eigen::Vector3d &p2);
    KDL::Frame toolFrame(robot_arm *arm);
};

/*
Folding controller with no estimation of the contact point

Uses values provided by the hardcoded contact point frame in order to know where
the contact point is
*/
class foldingControllerNoEstimate : public foldingControllerBase
{
  public:
    foldingControllerNoEstimate(ros::NodeHandle &n, robot_arm *arm_1, robot_arm *arm_2, double desiredNormalForce) : foldingControllerBase(n, arm_1, arm_2, desiredNormalForce){}
    void control(const double &vd, const double &wd, Eigen::Vector3d &vOut, Eigen::Vector3d &wOut);

  private:
    Eigen::Matrix3d Kp;
    double Kw;

    void updateR2();
    void updateSurfaceTangent();
    void updateSurfaceNormal();
    void updateTheta();
    void updateContactPoint();
    void updateVelocityTerm();
};

/*
Folding controller with contact point estimation

Implements the folding control strategy that feedback linearises the
contact point kinematics, using force torque measurements to compute
its location
*/
// class foldingControllerEstimate : public foldingControllerBase
// {
//   public:
//     foldingControllerEstimate(ros::NodeHandle &n, robot_arm *arm_1, robot_arm *arm_2, std::string grasp_point_1, std::string grasp_point_2, std::string base_frame_name) : foldingControllerBase(n, arm_1, arm_2, grasp_point_1, grasp_point_2, base_frame_name){}
//
//     void control(const Eigen::Vector3d &vd, const Eigen::Vector3d &wd, Eigen::Vector3d &vOut, Eigen::Vector3d &wOut);
//
//   private:
//     Eigen::Matrix3d Kp;
//     double Kw;
//
//     Eigen::Vector3d computeR2();
// };

/*
Folding controller with contact point estimation using the matricial form
of the control strategy
*/
// class foldingControllerMatrixEstimate : public foldingControllerBase
// {
//   public:
//     foldingControllerMatrixEstimate(ros::NodeHandle &n, robot_arm *arm_1, robot_arm *arm_2, std::string grasp_point_1, std::string grasp_point_2, std::string base_frame_name) : foldingControllerBase(n, arm_1, arm_2, grasp_point_1, grasp_point_2, base_frame_name){};
//     void control(const Eigen::Vector3d &vd, const Eigen::Vector3d &wd, Eigen::Vector3d &vOut, Eigen::Vector3d &wOut);
//
//   private:
//     Eigen::Vector3d computeR2();
// };

#endif
