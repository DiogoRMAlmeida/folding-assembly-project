#include <velocity_control_publisher/velocity_control_publisher.hpp>

velocityControlPublisher::velocityControlPublisher(ros::NodeHandle &n, robot_arm *robotArm, const char * controllerTopicName)
{
  arm = robotArm;

  if (n.hasParam("robot_playground/robotName"))
  {
    n.getParam("robotName", robotName);
  }
  else
  {
    ROS_WARN("No robot name has been provided");
    robotName = std::string("PR2");
  }

  if(robotName == std::string("PR2"))
  {
    pub = n.advertise<sensor_msgs::JointState>(controllerTopicName, 1);
  }
  else
  {
    pub = n.advertise<std_msgs::Float64MultiArray>(controllerTopicName, 1);
  }
}

void velocityControlPublisher::publishJointVelocityFloatArray(KDL::JntArray qVel)
{
  std_msgs::Float64MultiArray msg;

  for (int i = 0; i < qVel.rows(); i++)
  {
    msg.data.push_back(qVel(i));
  }

  pub.publish(msg);
}

void velocityControlPublisher::publishJointVelocityJointState(KDL::JntArray qVel)
{
  sensor_msgs::JointState msg;

  msg.header.stamp = ros::Time::now();

  for (int i = 0; i < qVel.rows(); i++)
  {
    msg.velocity.push_back(qVel(i));
  }

  pub.publish(msg);
}

/*
  Converts from the desired velocity on the tool frame to the velocity one needs
  to apply on the end effector frame (that will be commanded to the robot).

  Assumes static links between frames
*/
KDL::Twist velocityControlPublisher::fromFrameToEEF(KDL::Twist frameVel, std::string frame)
{
  tf::StampedTransform transform;
  KDL::Frame kdlTransform;
  KDL::Twist eefVel, tempVel;

  // 1 - Convert the requested velocity onto the end effector frame
  listener.lookupTransform(arm->getEndEffectorLinkFrame(), frame, ros::Time(0), transform);
  tf::transformTFToKDL(transform, kdlTransform);
  eefVel = kdlTransform*frameVel;

  // 2 - Align with world frame axis
  listener.lookupTransform(arm->getBaseLinkName(), arm->getEndEffectorLinkFrame(), ros::Time(0), transform);
  tf::transformTFToKDL(transform, kdlTransform);
  eefVel = kdlTransform.M*eefVel;
  // eefVel.vel = tempVel.vel;

  return eefVel;
}

/*
  Commands a desired tool frame velocity to the robot. To do this, the desired
  cartesian velocity for the tool frame is firstly converted into an appropriate
  velocity for the end effector.

  Inputs: cartesianVel: Desired cartesian velocity in the world frame
          frame: End-effector tool frame
*/
void velocityControlPublisher::publishCartesianVelocity(KDL::Twist cartesianVel, std::string frame)
{
  KDL::JntArray outputVelocities;
  KDL::Twist correctedVel;
  //
  // ROS_INFO("CART VEL");
  // for(int i = 0; i < 6; i++)
  //   std::cout << cartesianVel[i] << " ";
  // std::cout << std::endl;

  correctedVel = fromFrameToEEF(cartesianVel, frame);

  // ROS_INFO("CORR VEL");
  // for(int i = 0; i < 6; i++)
  //   std::cout << cartesianVel[i] << " ";
  // std::cout << std::endl;

  arm->kdl_wrapper->ik_solver_vel->setLambda(0.3);
  arm->kdl_wrapper->ik_solver_vel->CartToJnt(arm->jointValues(), correctedVel, outputVelocities);

  if(robotName == std::string("PR2"))
  {
    publishJointVelocityJointState(outputVelocities);
  }
  else
  {
    publishJointVelocityFloatArray(outputVelocities);
  }
}
