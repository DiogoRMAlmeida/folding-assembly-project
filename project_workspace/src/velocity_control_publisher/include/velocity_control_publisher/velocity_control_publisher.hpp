#ifndef __CONTROLLER_PUBLISHER__
#define __CONTROLLER_PUBLISHER__
#include <ros/ros.h>
#include <kdl_wrapper/kdl_wrapper.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>
#include <robot_arm/robot_arm.hpp>
#include <kdl/kdl.hpp>
#include <tf/transform_listener.h>
#include <Eigen/Dense>
#include <eigen_conversions/eigen_kdl.h>
#include <tf_conversions/tf_kdl.h>

class velocityControlPublisher
{
  public:
    velocityControlPublisher(ros::NodeHandle &n, robot_arm * robotArm, const char * controllerTopicName);
    void publishJointVelocityJointState(KDL::JntArray qVel);
    void publishJointVelocityFloatArray(KDL::JntArray qVel);
    void publishCartesianVelocity(KDL::Twist cartesianVel, std::string frame);

  private:
    robot_arm *arm;
    ros::Publisher pub;
    std::string robotName;
    tf::TransformListener listener;
    KDL::Twist fromFrameToEEF(KDL::Twist frameVel, std::string frame);
};

#endif
