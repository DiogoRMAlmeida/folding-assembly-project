#!/usr/bin/env python
import rospy
import rosbag
import os
import inspect
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import sys

def openBag():
    bag = rosbag.Bag(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + "/../bags/input.bag")

    #print bag

    for header in bag.read_messages(topics=['/odom']):
        print header[1].header

def printFigure(title, titleOffset, xVals, xMin, xMax, xScaleLabel, yScaleLabel, yVals, colors, yAxisFont, yLabels = [], enableGrid = False, legendPosition = "lower center", yMin = -1, yMax = -1, tickDensity = -1):
    plt.title(title, y = 1 + titleOffset)

    for i in range(len(yVals)):
        if len(yLabels) == len(yVals):
            plt.plot(xVals, yVals[i], colors[i], label = yLabels[i])
        else:
            plt.plot(xVals, yVals[i], colors[i])

    if yMin != -1 and yMax != -1:
        plt.ylim(yMin, yMax)

    plt.xlim(xMin, xMax)

    if len(yLabels) == len(yVals):
       plt.legend(loc = legendPosition, ncol = len(yVals))

    plt.xlabel(xScaleLabel)
    plt.ylabel(yScaleLabel, **yAxisFont)

    if tickDensity != -1:
        plt.xticks(np.arange(min(xVals), max(xVals), tickDensity))

    if enableGrid:
        plt.grid()

if __name__ == '__main__':
    try:
        bag = rosbag.Bag(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))) + "/../bags/input.bag")
        #print bag

        v1 = [[],[],[]]
        v1print = [[],[]]
        w1print = [[]]
        vf = [[],[],[]]
        w1 = [[],[],[]]
        p1 = [[],[],[]]
        vel1 = [[],[],[]]
        omega1 = [[],[],[]]
        p2 = [[],[],[]]
        pc = [[],[],[]]
        realPc = [[],[],[]]
        pd = [[],[],[]]
        f1 = [[],[],[]]
        f2 = [[],[],[]]
        torque1 = [[],[],[]]
        torque2 = [[],[],[]]
        surfaceNormal = [[],[],[]]
        thetaC = []
        realThetaC = []
        time = []

        for topic, msg, t in bag.read_messages():
            time = time + [t.to_sec()]
            p1[0] = p1[0] + [msg.p1.x]
            p1[1] = p1[1] + [msg.p1.y]
            p1[2] = p1[2] + [msg.p1.z]

            p2[0] = p2[0] + [msg.p2.x]
            p2[1] = p2[1] + [msg.p2.y]
            p2[2] = p2[2] + [msg.p2.z]

            v1[0] = v1[0] + [msg.v1.x]
            v1[1] = v1[1] + [msg.v1.y]
            v1[2] = v1[2] + [msg.v1.z]

            v1print[0] = v1print[0] + [(np.sign(msg.v1.y))*np.sqrt(msg.v1.x**2 + msg.v1.y**2)]
            v1print[1] = v1print[1] + [msg.v1.z]

            vel1[0] = vel1[0] + [msg.vel1.x]
            vel1[1] = vel1[1] + [msg.vel1.y]
            vel1[2] = vel1[2] + [msg.vel1.z]

            vf[0] = vf[0] + [msg.vf.x]
            vf[1] = vf[1] + [msg.vf.y]
            vf[2] = vf[2] + [msg.vf.z]

            pc[0] = pc[0] + [msg.pc.x]
            pc[1] = pc[1] + [msg.pc.y]
            pc[2] = pc[2] + [msg.pc.z]

            realPc[0] = realPc[0] + [msg.realPc.x]
            realPc[1] = realPc[1] + [msg.realPc.y + 0.02]
            realPc[2] = realPc[2] + [msg.realPc.z]

            pd[0] = pd[0] + [msg.pd.x]
            pd[1] = pd[1] + [msg.pd.y]
            pd[2] = pd[2] + [msg.pd.z]

            surfaceNormal[0] = surfaceNormal[0] + [msg.surfaceNormal.x]
            surfaceNormal[1] = surfaceNormal[1] + [msg.surfaceNormal.y]
            surfaceNormal[2] = surfaceNormal[2] + [msg.surfaceNormal.z]

            f2[0] = f2[0] + [msg.f2.x]
            f2[1] = f2[1] + [msg.f2.y]
            f2[2] = f2[2] + [msg.f2.z]

            f1[0] = f1[0] + [msg.f1.x]
            f1[1] = f1[1] + [msg.f1.y]
            f1[2] = f1[2] + [msg.f1.z]

            w1[0] = w1[0] + [msg.w1.x]
            w1[1] = w1[1] + [msg.w1.y]
            w1[2] = w1[2] + [msg.w1.z]

            w1print[0] = w1print[0] + [np.sqrt(msg.w1.x**2 + msg.w1.y**2 + msg.w1.z**2)]

            omega1[0] = omega1[0] + [msg.omega1.x]
            omega1[1] = omega1[1] + [msg.omega1.y]
            omega1[2] = omega1[2] + [msg.omega1.z]

            torque1[0] = torque1[0] + [msg.torque1.x]
            torque1[1] = torque1[1] + [msg.torque1.y]
            torque1[2] = torque1[2] + [msg.torque1.z]

            torque2[0] = torque2[0] + [msg.torque2.y]
            torque2[1] = torque2[1] + [msg.torque2.x]
            torque2[2] = torque2[2] + [msg.torque2.z]

            thetaC = thetaC + [-msg.thetaC]
            realThetaC = realThetaC + [-(msg.realThetaC) - 0.2 + 0.015]


        fontSize = 14
        axisSize = 18
        lineWidth = 1
        scaleFactor = 1.2
        savefig = 0

        if len(sys.argv) > 3:
            t1 = float(sys.argv[1])
            t2 = float(sys.argv[2])
            savefig = int(sys.argv[3])
        elif len(sys.argv) > 2:
            t1 = float(sys.argv[1])
            t2 = float(sys.argv[2])
        else:
            t1 = 0.
            t2 = np.max(time) - time[0] - 1

        font = {'size' : fontSize}
        axis_font = {'size' : axisSize}
        mpl.rcParams['lines.linewidth'] = lineWidth

        mpl.rc('font', **font)

        time = [time[i] - time[0] for i in range(len(time))]

        indexT1 = [ n for n,i in enumerate(time) if i > t1 ][0]
        indexT2 = [ n for n,i in enumerate(time) if i > t2 ][0]


        fig = plt.figure(figsize=(10,12))

        plt.subplot(321)
        printFigure("Velocity commands ($\mathbf{v}_1$)", 0.08, time, t1, t2, "Time (s)", "m/s", [v1print[0], v1print[1], np.array(pd[1])], ['r', 'b', 'k--'], axis_font, yLabels = ['$x$', '$y$', '$\mathbf{v}_d$'], enableGrid = True, tickDensity = 5)

        plt.subplot(322)
        printFigure("Angular velocity commands ($\omega_1 $)", 0.08, time, t1, t2, "Time (s)", "rad/s", [w1print[0]], ['k'], axis_font, enableGrid = True, tickDensity = 5)

        forceNormZ = np.apply_along_axis(np.linalg.norm, 0, np.array(f2)[::2])

        plt.subplot(323)
        printFigure("Force magnitude ($\mathbf{f}_2$)", 0.08, time, t1, t2, "Time (s)", "N", [forceNormZ, f2[1]], ['r', 'b'], axis_font, yLabels = ['$\mathbf{f}_{\perp}$', '$\mathbf{f}_\parallel$'], enableGrid = True, tickDensity = 5)

        torqueNorm = np.apply_along_axis(np.linalg.norm, 0, np.array(torque2))
        plt.subplot(324)
        printFigure("Torque magnitude (${\\tau}_2$)", 0.08, time, t1, t2, "Time (s)", "N/m", [torqueNorm], ['k'], axis_font, enableGrid = True, tickDensity = 5)

        plt.subplot(325)
        printFigure("Contact point ($\mathbf{p}_c$)", 0.08, time, t1, t2, "Time (s)", "m", [pc[1], realPc[1]], ['k', 'k--'], axis_font, yLabels = ["Computed", "Real"], enableGrid = True, tickDensity = 5)

        plt.subplot(326)
        printFigure("Rod piece orientation ($\\theta_C$)", 0.08, time, t1, t2, "Time (s)", "rad", [thetaC, realThetaC], ['k', 'k--'], axis_font, enableGrid = True, tickDensity = 5)

        fig.tight_layout()

        if savefig == 0:
            plt.show()
        else:
            plt.savefig("values.svg")


    except rospy.ROSInterruptException:
        pass
