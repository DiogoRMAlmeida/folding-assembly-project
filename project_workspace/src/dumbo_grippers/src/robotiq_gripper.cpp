#include "dumbo_grippers/robotiq_gripper.hpp"

robotiq_gripper::robotiq_gripper(std::string gripperTopic, float maxEffort) : gripper(gripperTopic, maxEffort)
{
  privateNh = ros::NodeHandle("~");
  pub = privateNh.advertise<robotiq_s_model_control::SModel_robot_output>(topic, 1);

  activate();
  setPinchMode();
}

void robotiq_gripper::fillMsg(robotiq_s_model_control::SModel_robot_output &msg, char rACT, char rMOD, char rGTO,
char rATR, char rGLV, char rICF, char rICS, char rPRA, char rSPA, char rFRA, char rPRB, char rSPB, char rFRB,
char rPRC, char rSPC, char rFRC, char rPRS, char rSPS, char rFRS)
{
  msg.rACT = rACT;
  msg.rMOD = rMOD;
  msg.rGTO = rGTO;
  msg.rATR = rATR;
  msg.rGLV = rGLV;
  msg.rICF = rICF;
  msg.rICS = rICS;
  msg.rPRA = rPRA;
  msg.rSPA = rSPA;
  msg.rFRA = rFRA;
  msg.rPRB = rPRB;
  msg.rSPB = rSPB;
  msg.rFRB = rFRB;
  msg.rPRC = rPRC;
  msg.rSPC = rSPC;
  msg.rFRC = rFRC;
  msg.rPRS = rPRS;
  msg.rSPS = rSPS;
  msg.rFRS = rFRS;
}

void robotiq_gripper::activate()
{
  robotiq_s_model_control::SModel_robot_output msg;

  fillMsg(msg, 1, 0, 1, 0, 0, 0, 0, 0, 255, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  pub.publish(msg);
}

void robotiq_gripper::setPinchMode()
{
  robotiq_s_model_control::SModel_robot_output msg;

  fillMsg(msg, 1, 1, 1, 0, 0, 0, 0, 0, 255, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  pub.publish(msg);
}

void robotiq_gripper::close()
{
  robotiq_s_model_control::SModel_robot_output msg;
  char closeVal = 255;

  fillMsg(msg, 1, 1, 1, 0, 0, 0, 0, closeVal, 255, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  pub.publish(msg);
}

void robotiq_gripper::open()
{
  robotiq_s_model_control::SModel_robot_output msg;

  fillMsg(msg, 1, 1, 1, 0, 0, 0, 0, 0, 255, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  pub.publish(msg);
}
