#include "dumbo_grippers/parallel_gripper.hpp"


parallel_gripper::parallel_gripper(std::string gripperTopic, float maxEffort) : gripper(gripperTopic, maxEffort)
{
  privateNh = ros::NodeHandle("~");
  pub = privateNh.advertise<control_msgs::GripperCommand>(topic, 1);
}
void parallel_gripper::close()
{
  control_msgs::GripperCommand msg;

  msg.position = 0.04; // Requires removal of the optoforce sensors
  msg.max_effort = maxEffort;

  pub.publish(msg);
}

void parallel_gripper::open()
{
  control_msgs::GripperCommand msg;

  msg.position = 0.0599; // mm
  msg.max_effort = maxEffort;

  pub.publish(msg);
}
