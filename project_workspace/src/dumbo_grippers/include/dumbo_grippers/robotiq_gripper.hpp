#ifndef __ROBOTIQ_GRIPPER__
#define __ROBOTIQ_GRIPPER__

#include <ros/ros.h>
#include <robot_arm/robot_arm.hpp>
#include <robotiq_s_model_control/SModel_robot_output.h>

class robotiq_gripper : public gripper
{
  public:
    robotiq_gripper(std::string gripperTopic, float maxEffort);
    ~robotiq_gripper();
    void open();
    void close();

  private:
    ros::NodeHandle privateNh;
    ros::Publisher pub;
    void activate();
    void setPinchMode();
    void fillMsg(robotiq_s_model_control::SModel_robot_output &msg, char rAct, char rMod, char rGTO,
    char rATR, char rGLV, char rICF, char rICS, char rPRA, char rSPA, char rFRA, char rPRB, char rSPB, char rFRB,
    char rPRC, char rSPC, char rFRC, char rPRS, char rSPS, char rFRS);
};

#endif
