#ifndef __PARALLEL_GRIPPER__
#define __PARALLEL_GRIPPER__

#include <ros/ros.h>
#include <robot_arm/robot_arm.hpp>
#include <control_msgs/GripperCommand.h>

class parallel_gripper : public gripper
{
  public:
    parallel_gripper(std::string gripperTopic, float maxEffort);
    ~parallel_gripper();
    void open();
    void close();

  private:
    ros::NodeHandle privateNh;
    ros::Publisher pub;
};

#endif
