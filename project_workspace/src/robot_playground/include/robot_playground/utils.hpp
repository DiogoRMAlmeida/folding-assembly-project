#ifndef __UTILS__
#define __UTILS__

#include <ros/ros.h>
#include <kdl/kdl.hpp>
#include <robot_arm/robot_arm.hpp>
#include <dumbo_grippers/robotiq_gripper.hpp>
#include <dumbo_grippers/parallel_gripper.hpp>
#include <pr2_mechanism_msgs/SwitchController.h>
#include <controller_manager_msgs/SwitchController.h>
#include <moveit/move_group_interface/move_group.h>
#include <robot_playground/gravityCompensation.hpp>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <velocity_control_publisher/velocity_control_publisher.hpp>

/*
  Class that will maintain all the helper methods for the playground execution.
*/
class utils
{
  public:
    utils(ros::NodeHandle &n);
    bool enablePositionControllers();
    bool enableVelocityControllers();
    bool loadPlaygroundParameters();
    bool moveArmsToInitialPose();

    robot_arm *arm1, *arm2;
    gripper *rightGripper, *leftGripper;
    std::string robotName;

  private:
    bool loadArm(std::string configName, robot_arm * &arm, gripper * &theGripper,
      moveit::planning_interface::MoveGroup * &theMoveGroup, GravityCompensationNode * &gravityCompensationNode,
      std::string moveGroupBaseLink, std::string &positionControllerName, std::string &velocityControllerName,
      std::string imuTopicName, geometry_msgs::PoseStamped &pose);
    bool loadInitialPose(std::string poseDir, geometry_msgs::PoseStamped &pose);
    bool loadGripper(std::string configName);
    ros::NodeHandle privateNh, publicNh;
    ros::ServiceClient controllerService;
    moveit::planning_interface::MoveGroup *rightArmGroup, *leftArmGroup;
    GravityCompensationNode *rightArmGravityCompensation, *leftArmGravityCompensation;
    geometry_msgs::PoseStamped leftInitialPose, rightInitialPose;
    std::string rightArmPositionControllerName, rightArmVelocityControllerName;
    std::string leftArmPositionControllerName, leftArmVelocityControllerName;
    robot_arm *rightArm, *leftArm;
    tf::TransformListener listener;
};
#endif
