/*
 *  gravity_compensation_node.cpp
 *
 *  Created on: Nov 12, 2013
 *  Authors:   Francisco Viña
 *            fevb <at> kth.se
 */

/* Copyright (c) 2013, Francisco Viña, CVAP, KTH
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of KTH nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL KTH BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <ros/ros.h>
#include <gravity_compensation/gravity_compensation.h>
#include <gravity_compensation/gravity_compensation_params.h>
#include <sensor_msgs/Imu.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <eigen_conversions/eigen_msg.h>
#include <boost/thread.hpp>
#include <std_srvs/Empty.h>


#ifndef __GRAVCOMP__
#define __GRAVCOMP__

class GravityCompensationNode
{
public:
	ros::NodeHandle n_;

	// subscribe to accelerometer (imu) readings
	ros::Subscriber topicSub_imu_;
	ros::Subscriber topicSub_ft_raw_;

	ros::Publisher topicPub_ft_zeroed_;
	ros::Publisher topicPub_ft_compensated_;

	ros::ServiceServer calibrate_bias_srv_server_;

	tf::TransformBroadcaster tf_br_;


	GravityCompensationNode(std::string imuTopic, std::string ftTopic, std::string arm);

	~GravityCompensationNode();

	bool getROSParameters();
	void topicCallback_imu(const sensor_msgs::Imu::ConstPtr &msg);

	void topicCallback_ft_raw(const geometry_msgs::WrenchStamped::ConstPtr &msg);

	// thread function for publishing the gripper center of mass transform
	void publish_gripper_com_tf();
	std::string getCompensatedTopicName();

	// only to be called when the robot is standing still and
	// while not holding anything / applying any forces
	bool calibrateBiasSrvCallback(std_srvs::Empty::Request &req,
																std_srvs::Empty::Response &res)
	{
		m_calibrate_bias = true;
		m_ft_bias = Eigen::Matrix<double, 6, 1>::Zero();
		return true;
	}

private:

	GravityCompensationParams *m_g_comp_params;
	GravityCompensation *m_g_comp;
	sensor_msgs::Imu m_imu;
	bool m_received_imu;
	double m_gripper_com_broadcast_frequency;
	bool m_calibrate_bias;
  unsigned int m_calib_measurements;
  Eigen::Matrix<double, 6, 1> m_ft_bias;
  std::string arm;
	std::string m_ft_frame_id;
	std::string zeroed_topic_name;
	std::string compensated_topic_name;
};
#endif
