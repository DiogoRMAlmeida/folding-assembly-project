#include <robot_playground/utils.hpp>

utils::utils(ros::NodeHandle &n)
{
  privateNh = ros::NodeHandle("~");
  publicNh = n;
  rightArm = NULL;
  leftArm = NULL;
}

bool utils::loadInitialPose(std::string poseDir, geometry_msgs::PoseStamped &pose)
{
  double roll, pitch, yaw;
  if(privateNh.hasParam(poseDir + std::string("position/x")))
  {
      privateNh.getParam(poseDir + std::string("position/x"), pose.pose.position.x);
  }
  else
  {
    ROS_ERROR("Missing x coordinate in %s", poseDir.c_str());
      privateNh.shutdown();
      return false;
  }

  if(privateNh.hasParam(poseDir + std::string("position/y")))
  {
      privateNh.getParam(poseDir + std::string("position/y"), pose.pose.position.y);
  }
  else
  {
    ROS_ERROR("Missing y coordinate in %s", poseDir.c_str());
      privateNh.shutdown();
      return false;
  }

  if(privateNh.hasParam(poseDir + std::string("position/z")))
  {
      privateNh.getParam(poseDir + std::string("position/z"), pose.pose.position.z);
  }
  else
  {
    ROS_ERROR("Missing z coordinate in %s", poseDir.c_str());
      privateNh.shutdown();
      return false;
  }

  if(privateNh.hasParam(poseDir + std::string("orientation/x")))
  {
      privateNh.getParam(poseDir + std::string("orientation/x"), roll);
  }
  else
  {
    ROS_ERROR("Missing x orientation coordinate in %s", poseDir.c_str());
      privateNh.shutdown();
      return false;
  }

  if(privateNh.hasParam(poseDir + std::string("orientation/y")))
  {
      privateNh.getParam(poseDir + std::string("orientation/y"), pitch);
  }
  else
  {
    ROS_ERROR("Missing y orientation coordinate in %s", poseDir.c_str());
      privateNh.shutdown();
      return false;
  }

  if(privateNh.hasParam(poseDir + std::string("orientation/z")))
  {
      privateNh.getParam(poseDir + std::string("orientation/z"), yaw);
  }
  else
  {
    ROS_ERROR("Missing z orientation coordinate in %s", poseDir.c_str());
      privateNh.shutdown();
      return false;
  }

  pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);
}

bool utils::loadArm(std::string configName, robot_arm * &arm, gripper * &theGripper,
  moveit::planning_interface::MoveGroup * &theMoveGroup, GravityCompensationNode * &gravityCompensationNode,
  std::string moveGroupBaseLink, std::string &positionControllerName, std::string &velocityControllerName,
  std::string imuTopicName, geometry_msgs::PoseStamped &pose)
{
  std::string configDir = std::string("config/") + configName + std::string("/MoveIt/");

  std::string moveGroupName;
  if(privateNh.hasParam(configDir + std::string("MoveGroup")))
  {
    privateNh.getParam(configDir + std::string("MoveGroup"), moveGroupName);
    theMoveGroup = new moveit::planning_interface::MoveGroup(moveGroupName);

    if(privateNh.hasParam(configDir + std::string("PositionTolerance")))
    {
      double tolerance;
      privateNh.getParam(configDir + std::string("PositionTolerance"), tolerance);

      theMoveGroup->setGoalPositionTolerance(tolerance);

      ROS_WARN("%s goal position tolerance set to %.5f!", configName.c_str(), tolerance);
    }

    if(privateNh.hasParam(configDir + std::string("OrientationTolerance")))
    {
      double tolerance;
      privateNh.getParam(configDir + std::string("OrientationTolerance"), tolerance);

      theMoveGroup->setGoalOrientationTolerance(tolerance);

      ROS_WARN("%s goal orientation tolerance set to %.5f!", configName.c_str(), tolerance);
    }
  }
  else
  {
    ROS_ERROR("No move group was provided in the parameter server for the %s"
      ". Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string moveGroupLinkName;
  if(privateNh.hasParam(configDir + std::string("MoveGroupLink")))
  {
    privateNh.getParam(configDir + std::string("MoveGroupLink"), moveGroupLinkName);
  }
  else
  {
    ROS_ERROR("No move group link was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string toolFrameName;
  if(privateNh.hasParam(configDir + std::string("ToolFrame")))
  {
    privateNh.getParam(configDir + std::string("ToolFrame"), toolFrameName);
  }
  else
  {
    ROS_ERROR("No tip link was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  configDir = std::string("config/") + configName + std::string("/");

  std::string gripperActionTopic;
  if(privateNh.hasParam(configDir + std::string("GripperActionTopic")))
  {
    privateNh.getParam(configDir + std::string("GripperActionTopic"), gripperActionTopic);
  }
  else
  {
    ROS_ERROR("No gripper action topic was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string gripperName;
  if(privateNh.hasParam(configDir + std::string("GripperName")))
  {
    privateNh.getParam(configDir + std::string("GripperName"), gripperName);
  }
  else
  {
    ROS_ERROR("No gripper name was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  double gripperMaxEffort;
  if(privateNh.hasParam(configDir + std::string("GripperMaxEffort")))
  {
    privateNh.getParam(configDir + std::string("GripperMaxEffort"), gripperMaxEffort);
  }
  else
  {
    ROS_ERROR("No gripper max effort was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string ftTopic;
  if(privateNh.hasParam(configDir + std::string("FtTopic")))
  {
    privateNh.getParam(configDir + std::string("FtTopic"), ftTopic);
  }
  else
  {
    ROS_ERROR("No force torque topic name was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string correctedFtTopic;
  if(privateNh.hasParam(configDir + std::string("CorrectedFtTopic")))
  {
    privateNh.getParam(configDir + std::string("CorrectedFtTopic"), correctedFtTopic);
  }
  else
  {
    ROS_ERROR("No force torque topic name was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string prefix;
  if(privateNh.hasParam(configDir + std::string("Prefix")))
  {
    privateNh.getParam(configDir + std::string("Prefix"), prefix);
  }
  else
  {
    ROS_ERROR("No prefix was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string armName;
  if(privateNh.hasParam(configDir + std::string("Name")))
  {
    privateNh.getParam(configDir + std::string("Name"), armName);
  }
  else
  {
    ROS_ERROR("No name was provided for the %s"
      " in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  std::string velocityControllerTopic;
  if(privateNh.hasParam(configDir + std::string("VelocityControllerTopic")))
  {
    privateNh.getParam(configDir + std::string("VelocityControllerTopic"), velocityControllerTopic);
  }
  else
  {
    ROS_ERROR("No velocity controller topic was provided for the %s"
      "in the parameter server. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  if(privateNh.hasParam(configDir + std::string("PositionControllerName")))
  {
    privateNh.getParam(configDir + std::string("PositionControllerName"), positionControllerName);
  }
  else
  {
    ROS_ERROR("No position controller name specified for the %s. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  if(privateNh.hasParam(configDir + std::string("VelocityControllerName")))
  {
    privateNh.getParam(configDir + std::string("VelocityControllerName"), velocityControllerName);
  }
  else
  {
    ROS_ERROR("No position controller name specified for the %s. Shutting down...", configName.c_str());
    privateNh.shutdown();
    return false;
  }

  arm = new robot_arm(publicNh, armName, moveGroupBaseLink, moveGroupLinkName, toolFrameName,
    velocityControllerTopic, correctedFtTopic);

  if (gripperName == std::string("robotiq"))
  {
    theGripper = new robotiq_gripper(gripperActionTopic, gripperMaxEffort);
  }
  else
  {
    if (gripperName == std::string("PG70"))
    {
      theGripper = new parallel_gripper(gripperActionTopic, gripperMaxEffort);
    }
    else{
      ROS_ERROR("GIVEN GRIPPER NAME FOR THE %s IS NOT CURRENTLY SUPPORTED! Shutting down...", configName.c_str());
      privateNh.shutdown();
      return false;
    }
  }

  gravityCompensationNode = new GravityCompensationNode(imuTopicName, ftTopic, prefix);
  gravityCompensationNode->getROSParameters();

  std:: string poseDir = configDir + std::string("StartingPose/");
  loadInitialPose(poseDir, pose);
  pose.header.frame_id = moveGroupBaseLink;
}

bool utils::loadPlaygroundParameters()
{
  if(privateNh.hasParam("robotName"))
  {
    privateNh.getParam("robotName", robotName);
  }
  else
  {
    ROS_ERROR("The parameter server should have a robotName parameter. Shutting down...");
    privateNh.shutdown();
    return false;
  }

  std::string imuTopicName;
  if(privateNh.hasParam("config/imuTopic"))
  {
    privateNh.getParam("config/imuTopic", imuTopicName);
  }
  else
  {
    ROS_ERROR("No IMU topic name was provided in the parameter server"
      "(config/imuTopic). Shutting down...");
    privateNh.shutdown();
    return false;
  }

  std::string moveGroupBaseLink;
  if(privateNh.hasParam("config/armsBaseLink"))
  {
    privateNh.getParam("config/armsBaseLink", moveGroupBaseLink);
  }
  else
  {
    ROS_ERROR("No base link was provided for the arms"
    "in the parameter server (config/armsBaseLink). Shutting down...");
    privateNh.shutdown();
    return false;
  }

  loadArm(std::string("right_arm"), rightArm, rightGripper, rightArmGroup, rightArmGravityCompensation,
  moveGroupBaseLink, rightArmPositionControllerName, rightArmVelocityControllerName,
  imuTopicName, rightInitialPose);
  loadArm(std::string("left_arm"), leftArm, leftGripper, leftArmGroup, leftArmGravityCompensation,
  moveGroupBaseLink, leftArmPositionControllerName, leftArmVelocityControllerName,
  imuTopicName, leftInitialPose);

  if(robotName == std::string("PR2"))
  {
    controllerService = publicNh.serviceClient<pr2_mechanism_msgs::SwitchController>("pr2_controller_manager/switch_controller");
  }
  else
  {
    controllerService = publicNh.serviceClient<controller_manager_msgs::SwitchController>("controller_manager/switch_controller");
  }

  std::string rodArm;
  if(privateNh.hasParam("config/rodArm"))
  {
    privateNh.getParam("config/rodArm", rodArm);
  }
  else
  {
    rodArm = std::string("right");
  }

  if (rodArm == std::string("right"))
  {
    arm1 = rightArm;
    arm2 = leftArm;
  }
  else
  {
    arm1 = leftArm;
    arm2 = rightArm;
  }

  ROS_DEBUG("Successfully loaded the playground configuration values");
  return true;
}

bool utils::enablePositionControllers()
{
  if(robotName == std::string("PR2"))
  {
    pr2_mechanism_msgs::SwitchController srv;

    srv.request.start_controllers.push_back(leftArmPositionControllerName);
    srv.request.start_controllers.push_back(rightArmPositionControllerName);
    srv.request.stop_controllers.push_back(leftArmVelocityControllerName);
    srv.request.stop_controllers.push_back(rightArmVelocityControllerName);
    srv.request.strictness = srv.request.STRICT;

    if (controllerService.call(srv))
    {
      return true;
    }

    ROS_ERROR("Failed to initialize position controllers. Shutting down...");
    privateNh.shutdown();
    return false;
  }
  else
  {
    controller_manager_msgs::SwitchController srv;
    srv.request.start_controllers.push_back(leftArmPositionControllerName);
    srv.request.start_controllers.push_back(rightArmPositionControllerName);
    srv.request.stop_controllers.push_back(leftArmVelocityControllerName);
    srv.request.stop_controllers.push_back(rightArmVelocityControllerName);
    srv.request.strictness = srv.request.STRICT;

    if (controllerService.call(srv))
    {
      return true;
    }

    ROS_ERROR("Failed to initialize position controllers. Shutting down...");
    privateNh.shutdown();
    return false;
  }
}

bool utils::enableVelocityControllers()
{
  if(robotName == std::string("PR2"))
  {
    pr2_mechanism_msgs::SwitchController srv;

    srv.request.stop_controllers.push_back(leftArmPositionControllerName);
    srv.request.stop_controllers.push_back(rightArmPositionControllerName);
    srv.request.start_controllers.push_back(leftArmVelocityControllerName);
    srv.request.start_controllers.push_back(rightArmVelocityControllerName);
    srv.request.strictness = srv.request.STRICT;

    if (controllerService.call(srv))
    {
      return true;
    }

    ROS_ERROR("Failed to initialize velocity controllers. Shutting down...");
    privateNh.shutdown();
    return false;
  }
  else
  {
    controller_manager_msgs::SwitchController srv;

    srv.request.stop_controllers.push_back(leftArmPositionControllerName);
    srv.request.stop_controllers.push_back(rightArmPositionControllerName);
    srv.request.start_controllers.push_back(leftArmVelocityControllerName);
    srv.request.start_controllers.push_back(rightArmVelocityControllerName);
    srv.request.strictness = srv.request.STRICT;

    if (controllerService.call(srv))
    {
      return true;
    }

    ROS_ERROR("Failed to initialize velocity controllers. Shutting down...");
    privateNh.shutdown();
    return false;
  }
}

bool utils::moveArmsToInitialPose()
{
  rightArmGroup->setPoseTarget(rightInitialPose.pose, rightArm->getEndEffectorLinkFrame());

  if(!rightArmGroup->move())
  {
    ROS_ERROR("Failed to plan a trajectory from the current state to the"
      "pose targets for the right arm! Shutting down...");
    // privateNh.shutdown();
    // return false;
  }

  ROS_DEBUG("Successfully moved the right arm to its initial pose");

  sleep(1.0);
  leftArmGroup->setPoseTarget(leftInitialPose.pose, leftArm->getEndEffectorLinkFrame());

  if(!leftArmGroup->move())
  {
    ROS_ERROR("Failed to plan a trajectory from the current state to the"
      "pose targets for the left arm! Shutting down...");
    // privateNh.shutdown();
    // return false;
  }

  ROS_DEBUG("Successfully moved the left arm to its initial pose");

  return true;
}
