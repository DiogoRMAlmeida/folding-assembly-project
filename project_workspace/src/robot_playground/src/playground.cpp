#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <robot_playground/utils.hpp>
#include <folding_assembly_controller/controller.hpp>
#include <tf_conversions/tf_kdl.h>

int setAlternateMotion(ros::Time &begin, double &vd, double &omegaD, double &normalForce)
{
  double vMax = 0.002, omegaMax = -0.01, t_step = 20.0;

  if((ros::Time::now() - begin).toSec() < t_step)
  {
    vd = 0;
    omegaD = omegaMax;
  }

  if((ros::Time::now() - begin).toSec() > t_step)
  {
    vd = vMax;
    omegaD = 0;
  }

  if((ros::Time::now() - begin).toSec() > 2*t_step)
  {
    vd = 0;
    omegaD = -2*omegaMax;
  }

  if((ros::Time::now() - begin).toSec() > 3*t_step)
  {
    vd = -vMax;
    omegaD = 0;
  }

  if((ros::Time::now() - begin).toSec() > 4*t_step)
  {
    vd = 0;
    omegaD = 2*omegaMax;
  }

  if((ros::Time::now() - begin).toSec() > 5*t_step)
  {
    vd = vMax;
    omegaD = 0;
  }

  if((ros::Time::now() - begin).toSec() > 6*t_step)
  {
    return -1;
  }
  return 0;
}

int setForceStep(ros::Time &begin, double &vd, double &omegaD, double &normalForce)
{
  if((ros::Time::now() - begin).toSec() > 10.0 && (ros::Time::now() - begin).toSec() < 11.0)
  {
    normalForce = 20;
    return 2;
  }

  if((ros::Time::now() - begin).toSec() > 20.0)
  {
    return -1;
  }

  normalForce = 0;
  return 0;
}


// Moves arm down until its measured force norm becomes bigger than a specified value
bool downUntilForce(velocityControlPublisher &velpub, KDL::Twist desiredCartesianVelocity, robot_arm &arm, double desiredForce)
{
  ros::Rate r(100);
  Eigen::Vector3d force;
  ROS_INFO("Moving down");
  bool break_cycle = false;
  while(!break_cycle)
  {
    velpub.publishCartesianVelocity(desiredCartesianVelocity, arm.getToolFrame());
    r.sleep();
    tf::vectorMsgToEigen(arm.getWrench().wrench.force, force);
    ROS_INFO("Force: %f", force.norm());
    if(force.norm() >= desiredForce)
    {
      break_cycle = true;
    }
  }
  KDL::Vector vel(0,0,0), rot(0,0,0);
  KDL::Twist finalVelocity(vel,rot);
  velpub.publishCartesianVelocity(finalVelocity, arm.getToolFrame());

  return true;
}

void eigenToMsg(Eigen::Vector3d &in, KDL::Vector &out)
{
  for(int i = 0; i < 3; i++)
  {
    out[i] = in[i];
  }
}

int main(int argc, char ** argv)
{
  ros::init(argc, argv, "playground");
  //SET DEBUG VERBOSITY
  if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) )
  {
     ros::console::notifyLoggerLevelsChanged();
  }

  ros::NodeHandle n;
  ros::AsyncSpinner spinner(1);
  ros::Rate r(100);
  utils utilObj(n);
  tf::TransformListener listener;

  spinner.start();

  utilObj.loadPlaygroundParameters();

  // use the position controllers
  if(!utilObj.enablePositionControllers())
  {
    return false;
  }

  ROS_INFO("Ready to move");

  if(!utilObj.moveArmsToInitialPose())
  {
    return false;
  }

  // utilObj.rightGripper->open();
  // utilObj.leftGripper->open();

  ROS_INFO("Place object on the right gripper and press enter");
  std::cin.get();
  // utilObj.rightGripper->close();

  ROS_INFO("Place object on the left gripper and press enter");
  std::cin.get();
  // utilObj.leftGripper->close();

  ROS_INFO("Place left gripper on the right position and press enter");
  std::cin.get();

  if(!utilObj.enableVelocityControllers())
  {
    return false;
  }

  // Contact control
  ROS_INFO("Press enter to establish contact");
  std::cin.get();
  ROS_INFO("Establishing contact");

  tf::StampedTransform transform;
  KDL::Frame frame;
  tf::transformTFToKDL(transform, frame);

  KDL::Vector vel(0.0 ,0.0, -0.01), rot(0,0,0);
  KDL::Twist desiredCartesianVelocity(vel,rot);
  KDL::JntArray outputVelocities;
  float force = -6;
  velocityControlPublisher *velpub;

  velpub = new velocityControlPublisher(n, utilObj.arm1, utilObj.arm1->getControllerTopic().c_str());
  sleep(1);

  if (!downUntilForce(*velpub, desiredCartesianVelocity, *(utilObj.arm2), force))
  {
    ROS_ERROR("Error when establishing contact. Aborting...");
    return false;
  }


  ROS_INFO("Contact established between parts. Press enter to execute folding controller");
  // End of contact control
  std::cin.get();

  // Folding controller initialization
  Eigen::Vector3d v1, w1, eigenTip;
  double Kw = 1, omegaD = 0.0, vd = 0.00, fn = 15;

  foldingControllerNoEstimate controller(n, utilObj.arm1, utilObj.arm2, fn);
  //foldingControllerMatrixEstimate controller(&leftArm, &rightArm, normal, tangent, n, rightForce, rightTorque, eigenTip);
  sleep(1.0);
  // End of folding controller initialization

  KDL::Twist u;
  KDL::Frame pFrame;

  ROS_INFO("Starting control...");
  //controller.setSurfaceNormal();
  //std::cin.get();

  ros::Time begin = ros::Time::now();

  int ret = 0;
  // double omegaStep = 0.10;
  while(ros::ok())
  {
    r.sleep();
    ret = setAlternateMotion(begin, vd, omegaD, fn);
    // ret = setForceStep(begin, vd, omegaD, fn);
    // if(ret == 2)
    // {
    //   controller.setDesiredNormalForce(fn);
    // }

    if(ret < 0)
    {
      ROS_INFO("Successfully ended the experiment :)");
      Eigen::Vector3d zero(0,0,0);
      eigenToMsg(zero, u.vel);
      eigenToMsg(zero, u.rot);

      if(!utilObj.enablePositionControllers())
      {
        return false;
      }

      if(!utilObj.moveArmsToInitialPose())
      {
        return false;
      }

      velpub->publishCartesianVelocity(u, utilObj.arm1->getBaseLinkName());
      return 0;
    }

    // Execute a control step
    controller.control(vd , omegaD, v1, w1);
    ROS_INFO("v1: %.5f, %.5f, %.5f\nw1: %.5f, %.5f, %.5f", v1(0), v1(1), v1(2), w1(0), w1(1), w1(2));

    // NEEDS TO CONVERT V1 AND W1 TO THE MOVEGROUPLINK FRAME BEFORE COMMANDING
    eigenToMsg(v1, u.vel);
    eigenToMsg(w1, u.rot);
    velpub->publishCartesianVelocity(u, utilObj.arm1->getBaseLinkName());

  }

  return 0;
}
