/*
 *  gravity_compensation_node.cpp
 *
 *  Created on: Nov 12, 2013
 *  Authors:   Francisco Viña
 *            fevb <at> kth.se
 */

/* Copyright (c) 2013, Francisco Viña, CVAP, KTH
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
      * Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      * Neither the name of KTH nor the
        names of its contributors may be used to endorse or promote products
        derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL KTH BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <ros/ros.h>
#include <robot_playground/gravityCompensation.hpp>
#include <gravity_compensation/gravity_compensation.h>
#include <gravity_compensation/gravity_compensation_params.h>
#include <sensor_msgs/Imu.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <eigen_conversions/eigen_msg.h>
#include <boost/thread.hpp>


	GravityCompensationNode::GravityCompensationNode(std::string imuTopic, std::string ftTopic, std::string arm)
	{
		n_ = ros::NodeHandle("~");
		m_g_comp_params  = new GravityCompensationParams();
		m_g_comp = NULL;
		m_received_imu = false;
		m_calibrate_bias = false;
    m_calib_measurements = 0;
    m_ft_bias = Eigen::Matrix<double, 6, 1>::Zero();
    this->arm = arm;

		ROS_INFO("Initializing gravity compensation node for %s arm", arm.c_str());
		// subscribe to accelerometer topic and raw F/T sensor topic
		topicSub_imu_ = n_.subscribe(imuTopic, 1, &GravityCompensationNode::topicCallback_imu, this);
		topicSub_ft_raw_ = n_.subscribe(ftTopic, 1, &GravityCompensationNode::topicCallback_ft_raw, this);

		// bias calibration service
    calibrate_bias_srv_server_ = n_.advertiseService("calibrate_bias", &GravityCompensationNode::calibrateBiasSrvCallback, this);

	  /// implementation of topics to publish
		zeroed_topic_name = "/" + arm + "_ft_zeroed";
		compensated_topic_name = "/" + arm + "_ft_compensated";
		topicPub_ft_zeroed_ = n_.advertise<geometry_msgs::WrenchStamped> (zeroed_topic_name, 10);
		topicPub_ft_compensated_ = n_.advertise<geometry_msgs::WrenchStamped> (compensated_topic_name, 10);
	}

	GravityCompensationNode::~GravityCompensationNode()
	{
		delete m_g_comp;
		delete m_g_comp_params;
	}

	bool GravityCompensationNode::getROSParameters()
	{
		/// Get F/T sensor bias
		XmlRpc::XmlRpcValue biasXmlRpc;
		Eigen::Matrix<double, 6, 1> bias;

		if (n_.hasParam((arm + std::string("_bias")).c_str()))
		{
			n_.getParam((arm + std::string("_bias")).c_str(), biasXmlRpc);
		}

		else
		{
			ROS_ERROR("Parameter '%s_bias' not set, shutting down node...", arm.c_str());
			n_.shutdown();
			return false;
		}

		if(biasXmlRpc.size()!=6)
		{
			ROS_ERROR("Invalid F/T bias parameter size (should be size 6), shutting down node");
			n_.shutdown();
			return false;
		}

		for (int i = 0; i < biasXmlRpc.size(); i++)
		{
			bias(i) = (double)biasXmlRpc[i];
		}


		// get the mass of the gripper
		double gripper_mass;
		if (n_.hasParam((arm + std::string("_gripper_mass")).c_str()))
		{
			n_.getParam((arm + std::string("_gripper_mass")).c_str(), gripper_mass);
		}

		else
		{
			ROS_ERROR("Parameter '%s_gripper_mass' not available", arm.c_str());
			n_.shutdown();
			return false;
		}

		if(gripper_mass<0.0)
		{
			ROS_ERROR("Parameter '%s_gripper_mass' < 0", arm.c_str());
			n_.shutdown();
			return false;
		}

		// get the pose of the COM of the gripper
		// we assume that it is fixed with respect to FT sensor frame
		// first get the frame ID
		tf::StampedTransform gripper_com;
		std::string gripper_com_frame_id;
		if (n_.hasParam((arm + std::string("_gripper_com_frame_id")).c_str()))
		{
			n_.getParam((arm + std::string("_gripper_com_frame_id")).c_str(), gripper_com_frame_id);
		}

		else
		{
			ROS_ERROR("Parameter '%s_gripper_com_frame_id' not available", arm.c_str());
			n_.shutdown();
			return false;
		}

		gripper_com.frame_id_ = gripper_com_frame_id;

		// now get the CHILD frame ID
		std::string gripper_com_child_frame_id;
		if (n_.hasParam((arm + std::string("_gripper_com_child_frame_id")).c_str()))
		{
			n_.getParam((arm + std::string("_gripper_com_child_frame_id")).c_str(), gripper_com_child_frame_id);
		}

		else
		{
			ROS_ERROR("Parameter '%s_gripper_com_child_frame_id' not available", arm.c_str());
			n_.shutdown();
			return false;
		}

		gripper_com.child_frame_id_ = gripper_com_child_frame_id;

		// now get the actual gripper COM pose
		Eigen::Matrix<double, 6, 1> gripper_com_pose;
		XmlRpc::XmlRpcValue gripper_com_pose_XmlRpc;
		if (n_.hasParam((arm + std::string("_gripper_com_pose")).c_str()))
		{
			n_.getParam((arm + std::string("_gripper_com_pose")).c_str(), gripper_com_pose_XmlRpc);
		}

		else
		{
			ROS_ERROR("Parameter '%s_gripper_com_pose' not set, shutting down node...", arm.c_str());
			n_.shutdown();
			return false;
		}

		if(gripper_com_pose_XmlRpc.size()!=6)
		{
			ROS_ERROR("Invalid 'gripper_com_pose' parameter size (should be size 6), shutting down node");
			n_.shutdown();
			return false;
		}

		if(n_.hasParam((arm + std::string("_force_torque_frame_id").c_str())))
		{
			n_.getParam((arm + std::string("_force_torque_frame_id")).c_str(), m_ft_frame_id);
		}
		else
		{
			ROS_ERROR("No %s_force_torque_frame_id parameter, shutting down node...", arm.c_str());
			n_.shutdown();
			return false;
		}

		for(unsigned int i=0; i<gripper_com_pose_XmlRpc.size(); i++)
		{
			gripper_com_pose(i) = gripper_com_pose_XmlRpc[i];
		}

		tf::Vector3 p;
		tf::vectorEigenToTF(gripper_com_pose.topRows(3), p);
		tf::Quaternion q;
		q.setRPY((double)gripper_com_pose(3),
				(double)gripper_com_pose(4),
				(double)gripper_com_pose(5));

		gripper_com = tf::StampedTransform(tf::Transform(q, p),
				ros::Time::now(),
				gripper_com_frame_id,
				gripper_com_child_frame_id);

		// get the publish frequency for the gripper
		// center of mass tf
		n_.param((arm + std::string("_gripper_com_broadcast_frequency")).c_str(),
				m_gripper_com_broadcast_frequency, 100.0);

		m_g_comp_params->setBias(bias);
		m_g_comp_params->setGripperMass(gripper_mass);
		m_g_comp_params->setGripperCOM(gripper_com);

		m_g_comp = new GravityCompensation(m_g_comp_params);

		return true;
	}

	void GravityCompensationNode::topicCallback_imu(const sensor_msgs::Imu::ConstPtr &msg)
	{
		m_imu = *msg;
		m_received_imu = true;
	}

	void GravityCompensationNode::topicCallback_ft_raw(const geometry_msgs::WrenchStamped::ConstPtr &msg)
	{
		static int error_msg_count=0;

		if(!m_received_imu || m_g_comp == NULL)
		{
			ROS_ERROR("No IMU or no m_g_comp");
			return;
		}

		if((ros::Time::now()-m_imu.header.stamp).toSec() > 0.5)
		{
			error_msg_count++;
			if(error_msg_count % 10==0)
				ROS_ERROR("Imu reading too old, not able to g-compensate ft measurement");
			return;
		}

		geometry_msgs::WrenchStamped ft_zeroed;
		ft_zeroed.header.frame_id = m_ft_frame_id;
		m_g_comp->Zero(*msg, ft_zeroed);
		topicPub_ft_zeroed_.publish(ft_zeroed);

		geometry_msgs::WrenchStamped ft_compensated;
		ft_zeroed.header.frame_id = m_ft_frame_id;
		m_g_comp->Compensate(ft_zeroed, m_imu, ft_compensated);

		if(m_calibrate_bias)
    {
        if(m_calib_measurements++<100)
        {
            m_ft_bias(0) += ft_compensated.wrench.force.x;
            m_ft_bias(1) += ft_compensated.wrench.force.y;
            m_ft_bias(2) += ft_compensated.wrench.force.z;
            m_ft_bias(3) += ft_compensated.wrench.torque.x;
            m_ft_bias(4) += ft_compensated.wrench.torque.y;
            m_ft_bias(5) += ft_compensated.wrench.torque.z;
        }

        // set the new bias
        if(m_calib_measurements == 100)
        {
            m_ft_bias = m_ft_bias/100;
            m_g_comp_params->setBias(m_g_comp_params->getBias() + m_ft_bias);
            m_calibrate_bias = false;
            m_calib_measurements = 0;
        }

    }

		topicPub_ft_compensated_.publish(ft_compensated);
	}

	std::string GravityCompensationNode::getCompensatedTopicName()
	{
		return compensated_topic_name;
	}
	// thread function for publishing the gripper center of mass transform
	void GravityCompensationNode::publish_gripper_com_tf()
	{
		static ros::Rate gripper_com_broadcast_rate(m_gripper_com_broadcast_frequency);
		try
		{
			while(ros::ok())
			{
				tf::StampedTransform gripper_com = m_g_comp_params->getGripperCOM();
				gripper_com.stamp_ = ros::Time::now();
				tf_br_.sendTransform(gripper_com);

				gripper_com_broadcast_rate.sleep();
			}
		}

		catch(boost::thread_interrupted&)
		{
			return;
		}
	}
