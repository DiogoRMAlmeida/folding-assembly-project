#include <pr2_controller_interface/controller.h>
#include <pr2_mechanism_model/joint.h>
#include <kdl_wrapper/kdl_wrapper.h>
#include <sensor_msgs/JointState.h>
#include <control_toolbox/pid.h>

namespace pr2Control
{
  class pr2VelocityController: public pr2_controller_interface::Controller
  {
    private:
      pr2_mechanism_model::RobotState *robot;
      std::vector<pr2_mechanism_model::JointState*> jointStates;
      std::vector<double> initVel;
      std::vector<double> goalVel;
      std::vector<control_toolbox::Pid> controllers;
      KDLWrapper * kdlWrapper;
      ros::Subscriber jointRefSub;
      ros::Time lastTime;

    public:
      virtual bool init(pr2_mechanism_model::RobotState *robot,
                       ros::NodeHandle &n);
      virtual void starting();
      virtual void update();
      virtual void stopping();
      void jointVelocityCallback(const sensor_msgs::JointStateConstPtr &msg);
  };
}
