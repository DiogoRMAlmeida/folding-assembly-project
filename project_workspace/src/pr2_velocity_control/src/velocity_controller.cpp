#include "pr2_velocity_control/velocity_controller.hpp"
#include <pluginlib/class_list_macros.h>


namespace pr2Control
  {
  /// Controller initialization in non-realtime
  bool pr2VelocityController::init(pr2_mechanism_model::RobotState *robot,
                              ros::NodeHandle &n)
  {
    std::string baseJoint, tipJoint, topicName;
    KDL::Chain kinChain;
    kdlWrapper = new KDLWrapper();

    if (!n.getParam("baseJoint", baseJoint))
    {
      ROS_ERROR("No base joint given in namespace: '%s')",
                n.getNamespace().c_str());
      return false;
    }

    if (!n.getParam("tipJoint", tipJoint))
    {
      ROS_ERROR("No tip joint given in namespace: '%s')",
                n.getNamespace().c_str());
      return false;
    }

    if(!kdlWrapper->init(baseJoint.c_str(), tipJoint.c_str()))
    {
        ROS_ERROR("Error initiliazing kdlWrapper");
    }

    kinChain = kdlWrapper->getKDLChain();
    this->robot = robot;
    jointStates.resize(kinChain.getNrOfJoints());
    goalVel.resize(kinChain.getNrOfJoints());
    initVel.resize(kinChain.getNrOfJoints());
    controllers.resize(kinChain.getNrOfJoints());

    int misses = 0;

    for (int i=0; i < kinChain.getNrOfJoints() + misses; i++)
    {
      if(kinChain.getSegment(i).getJoint().getType() != KDL::Joint::None)
      {
        std::stringstream stream;
        stream << "pidParameters" << i-misses+1;
        if (!controllers[i-misses].init(ros::NodeHandle(n, stream.str())))
        {
          ROS_ERROR("MyController could not construct PID controller for joint '%s'",
                    kinChain.getSegment(i).getJoint().getName().c_str());
          return false;
        }
        jointStates[i-misses] = robot->getJointState(kinChain.getSegment(i).getJoint().getName());

        if (!jointStates[i-misses])
        {
          ROS_ERROR("pr2VelocityController could not find joint named '%s'",
                    kinChain.getSegment(i).getJoint().getName().c_str());
          return false;
        }
      }
      else
      {
        misses++;
      }
    }

    if (!n.getParam("topicName", topicName))
    {
      ROS_ERROR("No subscription topic (topicName) given in namespace: '%s')",
                n.getNamespace().c_str());
      return false;
    }

    jointRefSub = n.subscribe(topicName.c_str(), 1, &pr2VelocityController::jointVelocityCallback, this);

    return true;
  }

  void pr2VelocityController::jointVelocityCallback(const sensor_msgs::JointStateConstPtr &msg)
  {
    ros::Duration dt = robot->getTime() - lastTime;

    // The outer loop is expect to run roughly 10 times slower than this controller
    if ((ros::Time::now() - msg->header.stamp).toSec() > 11*dt.toSec())
    {
      ROS_WARN("Large communication delay");
    }

    for(int i = 0; i < goalVel.size(); i++)
    {
      goalVel[i] = msg->velocity[i];
    }
  }

  /// Controller startup in realtime
  void pr2VelocityController::starting()
  {
    lastTime = robot->getTime();
    for (int i=0; i < jointStates.size(); i++)
    {
      initVel[i] = jointStates[i]->velocity_;
      goalVel[i] = jointStates[i]->velocity_;
      controllers[i].reset();
    }
  }

  /// Controller update loop in realtime
  void pr2VelocityController::update()
  {
    double currentVel;
    ros::Duration dt = robot->getTime() - lastTime;
    lastTime = robot->getTime();

    for (int i = 0; i < jointStates.size(); i++)
    {
      currentVel = jointStates[i]->velocity_;
      jointStates[i]->commanded_effort_ = controllers[i].updatePid(currentVel-goalVel[i], dt);
    }
  }

  /// Controller stopping in realtime
  void pr2VelocityController::stopping()
  {}

  /// Register controller to pluginlib
  PLUGINLIB_DECLARE_CLASS(pr2_velocity_control,leftArmPr2SpeedControl,
                           pr2Control::pr2VelocityController,
                           pr2_controller_interface::Controller)

  PLUGINLIB_DECLARE_CLASS(pr2_velocity_control,rightArmPr2SpeedControl,
                          pr2Control::pr2VelocityController,
                          pr2_controller_interface::Controller)
}
